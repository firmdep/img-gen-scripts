#!/bin/bash

set -e 
set -u

IID=${1}
WORKDIR=${IID}

if [ ! -e ${1}.tar.gz ]; then
	echo "Error: Cannot find tarball of rootfs for ${1}"
	exit 1
fi

mkdir "${WORKDIR}"
chmod a+rwx "${WORKDIR}"
chown -R ${USER} ${WORKDIR}
chgrp -R ${USER} ${WORKDIR}

IMAGE=${WORKDIR}/image.qcow2
echo "creating qemu image..."
qemu-img create -f qcow2 ${IMAGE} 1G
chmod a+rw ${IMAGE}

echo "mounting qemu image..."
sudo modprobe nbd max_part=8
sudo qemu-nbd --connect=/dev/nbd0 ${IMAGE} --nocache
sleep 1

echo "creating partition table..."
echo -e "o\nn\np\n1\n\n\nw" | fdisk /dev/nbd0
sleep 1

echo "creating filesystem..."
mkfs.ext2 /dev/nbd0p1

echo "making qemu image mountpoint..."
MP=${WORKDIR}/image
[ -e ${MP} ] || mkdir -p ${MP}
chown ${USER} ${MP}
echo "mounting QEMU image..."
mount /dev/nbd0p1 ${MP}

echo "extracting rootfs..."
tar -xf ${IID}.tar.gz -C ${MP}
mkdir -p ${MP}/chroot-tools
mkdir -p ${MP}/chroot-tools/libnvram
mkdir -p ${MP}/chroot-tools/libnvram.override

echo "patching filesystem(chroot)..."
cp $(which busybox) ${MP}
cp fiximage.sh ${MP}
chroot ${MP} /busybox ash /fiximage.sh
rm ${MP}/busybox
rm ${MP}/fiximage.sh

echo "unmounting qemu image..."
sync
rsync -av --exclude dev ${MP} ${WORKDIR}/tmp
mv ${WORKDIR}/tmp/image ${WORKDIR}/rootfs
rmdir ${WORKDIR}/tmp
umount ${MP}
sudo qemu-nbd --disconnect /dev/nbd0

